#include "Checker.h"

int getLen(char s[256]){
    int i;
    for (i = 0; s[i] != '\0'; ++i);
    return i;
}

int checkEmailFormat(char email[256]){
    if(!email){
        printf("%s\n", "You didn't put an email");
        return 0;
    }
    if (email == 0){
        return 0;
    }
    int atIdx = 0;
    int dotIdx = 0;
    int i;

    for(i = 0; i < getLen(email); i++){
        // if '.' preceeds '@' return false
        if (email[i] == '.' && atIdx == 0){
            return 0;
        }
        if (email[i] == '.'){
            if(i-dotIdx == 1){
                return 0;
            }
            dotIdx = i;
        }
        // find index of @
        if(email[i] == '@'){
        // if there is more than one '@' return false
            if(atIdx){
                return 0;
            }
            atIdx = i;
            continue;
        }
        // if there is more than one '@' return false
    }
    // if there is no @ or @ is in the beginning or end return false
    if(atIdx == 0 || atIdx == getLen(email)){
        return 0;
    }
    // if there is no "." return false
    if(dotIdx == 0){
        return 0;
    }
    // if . is directly after @ return false
    if(email[atIdx+1] == '.'){
        return 0;
    }
    // return checkDot(email, atIdx);
    return 1;
}
